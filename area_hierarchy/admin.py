from django.contrib import admin

from area_hierarchy.models import District
from .models import District, SubDistrict
# Register your models here.
admin.site.register(District)
admin.site.register(SubDistrict)