# Generated by Django 3.2.8 on 2021-11-02 10:56

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('area_hierarchy', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='subdistrict',
            name='country',
            field=models.ForeignKey(on_delete=django.db.models.deletion.RESTRICT, to='area_hierarchy.district'),
        ),
    ]
