# Generated by Django 3.2.8 on 2021-11-05 08:16

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('area_hierarchy', '0008_auto_20211105_0814'),
    ]

    operations = [
        migrations.RenameField(
            model_name='district',
            old_name='district_census_id',
            new_name='census_id',
        ),
        migrations.RenameField(
            model_name='district',
            old_name='district_name',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='subdistrict',
            old_name='taluka_census_id',
            new_name='census_id',
        ),
        migrations.RenameField(
            model_name='subdistrict',
            old_name='taluka_name',
            new_name='name',
        ),
    ]
