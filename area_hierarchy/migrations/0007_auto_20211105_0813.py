# Generated by Django 3.2.8 on 2021-11-05 08:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('area_hierarchy', '0006_auto_20211105_0812'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='district',
            options={},
        ),
        migrations.AlterModelOptions(
            name='subdistrict',
            options={},
        ),
        migrations.AddField(
            model_name='subdistrict',
            name='census_id',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='district',
            name='census_id',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='district',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='district',
            name='name',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='subdistrict',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
        migrations.AlterField(
            model_name='subdistrict',
            name='name',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterModelTable(
            name='district',
            table=None,
        ),
        migrations.AlterModelTable(
            name='subdistrict',
            table=None,
        ),
    ]
