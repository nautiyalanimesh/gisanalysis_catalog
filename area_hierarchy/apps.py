from django.apps import AppConfig


class AreaHierarchyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'area_hierarchy'
