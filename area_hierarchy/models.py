from django.db import models

# Create your models here.

# class District(models.Model):
#     name = models.CharField(max_length=30)
#     census_id = models.CharField(max_length=30)

#     def __str__(self):
#         return self.name

# class SubDistrict(models.Model):
#     district = models.ForeignKey(District, on_delete=models.RESTRICT)
#     name = models.CharField(max_length=30)
#     models.CharField(max_length=30)

#     def __str__(self):
#         return self.name

class District(models.Model):
    id = models.IntegerField(primary_key=True)
    census_id = models.CharField(max_length=50, blank=True, null=True)
    name = models.CharField(max_length=50, blank=True, null=True)
    
    def __str__(self):
        return self.name

    # class Meta:
    #     managed = False
    #     db_table = 'district'


class SubDistrict(models.Model):
    id = models.IntegerField(primary_key=True)
    district = models.ForeignKey(District, on_delete=models.RESTRICT)
    census_id = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
            return self.name

    # class Meta:
    #     managed = False
    #     db_table = 'taluka'