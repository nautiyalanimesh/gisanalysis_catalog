from django import forms
from .models import Resources

class PortalForms(forms.ModelForm):
    class Meta:
        model = Resources
        fields = [
            'district',
            'subdistrict',
            'title',
            'description',
            'tags',
            'document',
        ]