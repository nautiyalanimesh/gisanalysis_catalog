# Generated by Django 3.2.8 on 2021-11-02 07:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0002_auto_20211031_0727'),
    ]

    operations = [
        migrations.AlterField(
            model_name='portal',
            name='document',
            field=models.FileField(null=True, upload_to='document/%Y/%m/%d'),
        ),
    ]
