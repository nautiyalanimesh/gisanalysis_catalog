# Generated by Django 3.2.8 on 2021-11-07 09:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('portal', '0006_rename_taluka_resources_subdistrict'),
    ]

    operations = [
        migrations.AddField(
            model_name='resources',
            name='type',
            field=models.CharField(choices=[('DOC', 'doc'), ('IMG', 'image'), ('SHP', 'shpfile')], max_length=3, null=True),
        ),
    ]
