from django.db import models
from area_hierarchy.models import District, SubDistrict
from taggit.managers import TaggableManager

RESOURCES_TYPE = [
    ('IMG', 'Image'),
    ('DOC', 'Document'),
    ('SHP', 'Spatial Geometry'),
    ('CSV', 'Data'),
]

DATA_DOMAIN = [
    ('CENS','Census 2011'),
    ('ROAD','Road Network')
]
class Resources(models.Model):
    district = models.ForeignKey(District, on_delete=models.SET_NULL, null=True)
    subdistrict = models.ForeignKey(SubDistrict, on_delete=models.SET_NULL, null=True,default=0,blank=True)
    title = models.CharField(max_length=250)
    document = models.FileField(upload_to='document/%Y/%m/%d',null=True)
    description = models.TextField(null = True)
    published = models.DateField(auto_now_add=True,null=True)
    slug = models.SlugField(unique=True, max_length=100)
    type = models.CharField(max_length=3,choices=RESOURCES_TYPE)
    data_domain = models.CharField(max_length=4,choices=DATA_DOMAIN)
    tags = TaggableManager()

    def getCategoryList(self):
        return {**dict(RESOURCES_TYPE) ,**dict(DATA_DOMAIN)}  #

        
    def __str__(self):
        return self.title