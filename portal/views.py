from django.shortcuts import render
from .models import Resources
from .forms import PortalForms
from taggit.models import Tag
from django.template.defaultfilters import slugify
from django.contrib import messages

from django.shortcuts import get_object_or_404


#for dropdown
from area_hierarchy.models import SubDistrict, District
from django.core import serializers
from django.http import HttpResponse


def input_view(req):
    if req.is_ajax():
        x = req.GET['district_id']
        selected_subdistricts = SubDistrict.objects.filter(district_id=int(x))
        qs_json = serializers.serialize('json', selected_subdistricts)
        return HttpResponse(qs_json, content_type='application/json')

    posts = Resources.objects.all()
    districts = District.objects.all()

    common_tags = Resources.tags.most_common()[:4]
    form = PortalForms(req.POST, req.FILES)

   
    if form.is_valid():
        newpost = form.save(commit=False)
        newpost.slug = slugify(newpost.title)
        newpost.save()
        form.save_m2m()
        messages.add_message(req, messages.INFO, 'Resource Added')
    
    else:
        for fields in form:
            print(fields.name,fields.errors)
        
    context = {
        'posts':posts,
        'common_tags':common_tags,
        'form':form,
        'districts':districts
    }
    return render(req, 'portal/home.html', context)

# def detail_view(request, slug):
#     post = get_object_or_404(Post, slug=slug)
#     context = {
#         'post':post,
#     }
#     return render(request, 'detail.html', context)

def tagged(request, slug):
    tag = get_object_or_404(Tag, slug=slug)
    # print(tag)

    # try:
    #     tag = Tag.objects.get(slug=slug)
    # except Tag.DoesNotExist:
    #     raise Http404("No MyModel matches the given query.")

    common_tags = Resources.tags.most_common()[:4]
    posts = Resources.objects.filter(tags=tag)
    context = {
        'tag':tag,
        'common_tags':common_tags,
        'posts':posts,
    }
    return render(request, 'home/index.html', context)