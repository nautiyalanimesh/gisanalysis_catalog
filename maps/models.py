from django.contrib.gis.db import models
from django.core.serializers import serialize
from functools import reduce
import operator
from django.db.models import Q

# Create your models here.

class AllIndiaDistricts(models.Model):
    id = models.IntegerField(primary_key=True)
    geom = models.MultiPolygonField(blank=True, null=True)
    fid_2 = models.IntegerField(blank=True, null=True)
    fid_1 = models.IntegerField(blank=True, null=True)
    fid = models.IntegerField(blank=True, null=True)
    district = models.CharField(max_length=254, blank=True, null=True)
    state = models.CharField(max_length=254, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'all_india_districts_11june2020'

    def getKokanDist(self):
        geometry = AllIndiaDistricts.objects.filter(
        reduce(operator.or_, (Q(district__icontains=x) for x in ['Thane','Palghar','Mumbai Suburban','Mumbai City','Raigad','Sindhudurg','Ratnagiri']))
        )
        return serialize('geojson',geometry)



class MahaTaluka(models.Model):
    geom = models.GeometryField(blank=True, null=True)
    lgd_distco = models.CharField(max_length=3, blank=True, null=True)
    distname = models.CharField(max_length=50, blank=True, null=True)
    taluka_cod = models.DecimalField(max_digits=65535, decimal_places=65535, blank=True, null=True)
    taluka_nam = models.CharField(max_length=254, blank=True, null=True)
    tribal_po = models.CharField(max_length=100, blank=True, null=True)
    atc = models.CharField(max_length=100, blank=True, null=True)
    field_2011_dist = models.BigIntegerField(db_column='_2011-dist', blank=True, null=True)  # Field renamed to remove unsuitable characters. Field renamed because it started with '_'.

    class Meta:
        managed = False
        db_table = 'maha_taluka_13march2021'

    def getTaluka(self,dist_name):
        qs = MahaTaluka.objects.filter(distname__icontains = dist_name)
        return serialize('geojson',qs)
