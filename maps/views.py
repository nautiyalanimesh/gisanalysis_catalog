from django.http.response import HttpResponsePermanentRedirect
from django.shortcuts import render
from django.http import HttpResponse
from maps.models import AllIndiaDistricts,MahaTaluka
import geopandas as gpd
import tempfile
from zipfile import ZipFile
import os


import json

# Create your views here.

def export_shp(req,district):
    mahaTaluka = MahaTaluka()
    geom = mahaTaluka.getTaluka(district)
    geom = json.loads(geom)
    gdf = gpd.GeoDataFrame.from_features(geom['features'])
    basename = district

    with tempfile.TemporaryDirectory() as tmp_dir:

            gdf.to_file(os.path.join(tmp_dir, f'{basename}.shp'), driver='ESRI Shapefile')

            # Zip the exported files to a single file
            tmp_zip_file_name = f'{basename}.zip'
            tmp_zip_file_path = f"{tmp_dir}/{tmp_zip_file_name}"
            tmp_zip_obj = ZipFile(tmp_zip_file_path, 'w')

            for file in os.listdir(tmp_dir):
                if file != tmp_zip_file_name:
                    tmp_zip_obj.write(os.path.join(tmp_dir, file), file)

            tmp_zip_obj.close()

        # Return the zip file as download response and delete it afterwards
            try:
                with open(tmp_zip_file_path, 'rb') as file:
                    response = HttpResponse(file, content_type='application/force-download')
                    response['Content-Disposition'] = f'attachment; filename="{tmp_zip_file_name}"'
                    return response
            finally:
                os.remove(tmp_zip_file_path)
    # res = HttpResponse(geom, content_type='application/json')
    # res['Content-Disposition'] = 'attachment; filename="'+district+'.geojson"'
    # return res

def export_district(req):
    allIndDist = AllIndiaDistricts()
    kokanDist = allIndDist.getKokanDist()
    kokanDist = json.loads(kokanDist)
    gdf = gpd.GeoDataFrame.from_features(kokanDist['features'])
    basename = 'kokan_dist'

    with tempfile.TemporaryDirectory() as tmp_dir:

        gdf.to_file(os.path.join(tmp_dir, f'{basename}.shp'), driver='ESRI Shapefile')

        # Zip the exported files to a single file
        tmp_zip_file_name = f'{basename}.zip'
        tmp_zip_file_path = f"{tmp_dir}/{tmp_zip_file_name}"
        tmp_zip_obj = ZipFile(tmp_zip_file_path, 'w')

        for file in os.listdir(tmp_dir):
            if file != tmp_zip_file_name:
                tmp_zip_obj.write(os.path.join(tmp_dir, file), file)

        tmp_zip_obj.close()

     # Return the zip file as download response and delete it afterwards
        try:
            with open(tmp_zip_file_path, 'rb') as file:
                response = HttpResponse(file, content_type='application/force-download')
                response['Content-Disposition'] = f'attachment; filename="{tmp_zip_file_name}"'
                return response
        finally:
            os.remove(tmp_zip_file_path)