from django.urls import path, re_path
from .views import export_shp, export_district
# ,detail_view,tagged
urlpatterns = [
    path('export/<str:district>', export_shp, name="export-taluka"),
    path('export/', export_district, name="export-district"),
]
