from operator import sub
from django.shortcuts import render
from area_hierarchy.models import SubDistrict, District
from portal.models import Resources
from django.contrib import messages
from django.core import serializers
from django.http import HttpResponse
from django.db.models import Q

from django.core.serializers import serialize
from maps.models import AllIndiaDistricts, MahaTaluka
from functools import reduce
import operator
from django.contrib.postgres.search import SearchVector, SearchQuery, SearchHeadline
from django.shortcuts import get_object_or_404
from taggit.models import Tag



allIndiaDistrict = AllIndiaDistricts()
districts = District.objects.all()

category = Resources()
types = category.getCategoryList()


def home(req):
    talukaGeom = MahaTaluka()
    sub_district_name = ""
    district_name=""


    if req.is_ajax():
        x = req.GET['district_id']
        subdistricts_list = SubDistrict.objects.filter(district_id=int(x))
        qs_json = serializers.serialize('json', subdistricts_list)
        return HttpResponse(qs_json, content_type='application/json')


    if req.method == 'GET':
        search_query = req.GET.get('search_query',None)
        sel_district = req.GET.get('district',None)
        sel_subdistrict = req.GET.get('subdistrict',None)
        sel_type = req.GET.getlist('type',None)
        sel_tag = req.GET.get('tag',None)
        print("search _query :%s \n district : %s \n subdistrict : %s \n type : %s \n tag :%s" % (search_query,sel_district,sel_subdistrict,sel_type,sel_tag))
        
        # get boundries
        if sel_district == "-1" or sel_district == None or sel_subdistrict == None or sel_subdistrict == "":
            geojson = allIndiaDistrict.getKokanDist()
        else:
            if(sel_district != "-1"):
                district_name = districts.filter(id=sel_district).values()[0]['name'].lower()
            if(sel_subdistrict != "-1"):
                sub_district_name = SubDistrict.objects.filter(id=sel_subdistrict).values()[0]['name'].lower()
            geojson = talukaGeom.getTaluka(district_name)
    
        result = None
        if((not search_query) and (not sel_district or sel_district == "-1") and (not sel_type)):
            result = Resources.objects.all()

        if(sel_tag):
            tag = get_object_or_404(Tag, slug=sel_tag)
            result = Resources.objects.filter(tags=tag)
        else:
            if(search_query and (sel_district and sel_district != "-1")): #if both seach query and district are entered
                result = Resources.objects.filter(
                        Q(district__id__icontains=sel_district) & Q(subdistrict__id__icontains=sel_subdistrict)
                    ).filter(tags__name__in=search_query.split())
            elif(search_query): #if only search query is entered
                query = SearchQuery(search_query)
                vector = SearchVector('title','description')
                result = Resources.objects.annotate(search=vector,headline = SearchHeadline('description',query)).filter(search=query)

                # result = Resources.objects.filter(tags__name__in=search_query.split())
            elif(sel_district and sel_district != "-1"): # if only district is entered
                result = Resources.objects.filter(
                        Q(district__id__icontains=sel_district) & (Q(subdistrict__id__icontains=sel_subdistrict ) | Q(subdistrict__id__isnull=True ))
                    )
            if sel_type:
                if not result:
                    result = Resources.objects.all()
                result = result.filter(reduce(operator.or_, ((Q(type__icontains=x) | Q(data_domain__icontains=x)) for x in sel_type)))

        if not result:
            messages.add_message(req,messages.INFO,'No Resource found')
            # result = Resources.objects.all()



    context ={
        'posts':result,
        'districts': districts,
        'types':types,
        'search_query': search_query,
        'sel_district' : sel_district,
        'sel_subdistrict': sel_subdistrict,
        'sel_types': sel_type,
        'geojson': geojson,
        'sel_subdist_name': sub_district_name,
        'district_name':district_name,
    }
    return render(req,"home/index.html",context)


# def fetch_subdistrict(request):
#     district_id = request.GET.get('district_name')
#     sub_district = SubDistrict.objects.filter(district_id=district_id).order_by('name')
#     return render(request, 'home/index.html', {'taluka': sub_district})

# def tagged(req, slug):
#     tag = get_object_or_404(Tag, slug=slug)
#     result = Resources.objects.filter(tags=tag)
    
#     if req.is_ajax():
#         x = req.GET['district_id']
#         subdistricts_list = SubDistrict.objects.filter(district_id=int(x))
#         qs_json = serializers.serialize('json', subdistricts_list)
#         return HttpResponse(qs_json, content_type='application/json')

#     context = {
#         'posts':result,
#         'districts':districts,
#         'types':types
#     }
#     return render(req, 'home/index.html', context)