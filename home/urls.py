from django.urls import path
from .views import home
from django.views.generic import TemplateView

# load_taluka


# ,detail_view,tagged
urlpatterns = [
    path('', home, name="home"),
    # path('tag/<slug:slug>/', tagged, name="tagged"),

    # path('test/', TemplateView.as_view(template_name="home/test.html")),
    # path('test2/', TemplateView.as_view(template_name="home/test2.html")),
    # path('test3/', TemplateView.as_view(template_name="home/test3.html")),

    # path('ajax/load-taluka/', load_taluka, name='ajax_load_taluka'),  # <-- this one here

]
